package app.controller;

import app.service.SecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/controller1")
public class FirstController {

    @Autowired
    private SecondService secondService;

    @GetMapping
    public ModelAndView doSomething() {
        System.out.println("Primul meu Controller MVC ");
        int x= 13;
        int y = 13;
        int result = secondService.doSomething(x, y);
        ModelAndView mav = new ModelAndView("controller1");
        mav.addObject("result", result);
        return mav;
    }

}
