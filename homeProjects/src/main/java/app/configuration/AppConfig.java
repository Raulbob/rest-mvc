package app.configuration;

import app.service.SecondService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "app")
public class AppConfig {


    @Bean
    public SecondService firstService() {
        return new SecondService();
    }

}
