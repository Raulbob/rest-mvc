package app.webservice;

import app.service.SecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstRest {

    @Autowired
    private SecondService secondService;

    @RequestMapping("/rest1")
    public String doSomething() {
        System.out.println("Primul meu Rest WebService ");
        int x= 13;
        int y = 13;
        int result = secondService.doSomething(x, y);
        return "" + result;
    }

}
