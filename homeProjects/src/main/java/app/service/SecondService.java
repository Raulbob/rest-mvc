package app.service;

import org.springframework.stereotype.Service;

@Service
public class SecondService {

    public int doSomething(int x, int y) {
        return x*y;
    }

}
